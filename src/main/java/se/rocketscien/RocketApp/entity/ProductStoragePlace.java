package se.rocketscien.RocketApp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@IdClass(ProductStoragePlace.PrimaryKey.class)
public class ProductStoragePlace {

    @Data
    public static class PrimaryKey implements Serializable {

        private Long product;

        private Long storagePlace;
    }

    @Id
    @ManyToOne
    private Product product;

    @Id
    @ManyToOne
    private StoragePlace storagePlace;

    private Integer productAmount;

    public ProductStoragePlace(Product product, StoragePlace storagePlace, Integer productAmount) {
        this.product = product;
        this.storagePlace = storagePlace;
        this.productAmount = productAmount;
    }
}
