package se.rocketscien.RocketApp.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class StoragePlace extends BaseEntity {
    @ManyToOne(cascade = CascadeType.MERGE)
    private Warehouse warehouse;

    private Integer rowNumber;

    private Integer placeNumber;

    public StoragePlace(Warehouse warehouse, Integer rowNumber, Integer placeNumber) {
        this.warehouse = warehouse;
        this.rowNumber = rowNumber;
        this.placeNumber = placeNumber;
    }
}
