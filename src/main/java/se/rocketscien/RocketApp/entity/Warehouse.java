package se.rocketscien.RocketApp.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class Warehouse extends BaseEntity {
    @ManyToOne(cascade = CascadeType.MERGE)
    private Address address;

    public Warehouse(Address address) {
        this.address = address;
    }
}
