package se.rocketscien.RocketApp.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class Product extends BaseEntity {
    private String name;

    private BigDecimal rating;

    public Product(String name) {
        this.name = name;
    }
}
