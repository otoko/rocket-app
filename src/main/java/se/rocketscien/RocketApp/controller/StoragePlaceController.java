package se.rocketscien.RocketApp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.RocketApp.converter.ResponseConverter;
import se.rocketscien.RocketApp.dto.request.StoragePlaceCreateRequestDto;
import se.rocketscien.RocketApp.dto.response.StoragePlaceCreateResponseDto;
import se.rocketscien.RocketApp.entity.StoragePlace;
import se.rocketscien.RocketApp.service.WarehouseService;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import javax.validation.Valid;

@RestController
@RequestMapping("storagePlace")
@RequiredArgsConstructor
public class StoragePlaceController {

    private final WarehouseService warehouseService;

    private final ResponseConverter responseConverter;

    @ApiOperation(
            httpMethod = "POST",
            value = "Заведение нового места хранения на складе"
    )
    @PostMapping("create")
    public ResponseDto<StoragePlaceCreateResponseDto> createStoragePlace(
            @ApiParam(
                    required = true,
                    value = "Запрос с параметрами места хранения"
            )
            @RequestBody @Valid final StoragePlaceCreateRequestDto requestDto) throws ServiceException {
        StoragePlace result = warehouseService.addStoragePlace(
                requestDto.getWarehouseId(),
                requestDto.getRowNumber(),
                requestDto.getPlaceNumber()
        );
        return ResponseDto.of(
                responseConverter.toDto(result)
        );
    }
}
