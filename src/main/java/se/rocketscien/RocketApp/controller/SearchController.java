package se.rocketscien.RocketApp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.RocketApp.converter.DataConverter;
import se.rocketscien.RocketApp.converter.RequestConverter;
import se.rocketscien.RocketApp.converter.ResponseConverter;
import se.rocketscien.RocketApp.dto.ProductInfoDto;
import se.rocketscien.RocketApp.dto.request.ProductsFindRequestDto;
import se.rocketscien.RocketApp.dto.response.ProductsFindResponseDto;
import se.rocketscien.RocketApp.entity.ProductStoragePlace;
import se.rocketscien.RocketApp.service.ProductPriceService;
import se.rocketscien.RocketApp.service.WarehouseService;
import se.rocketscien.RocketApp.service.search.SearchFilter;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequiredArgsConstructor
public class SearchController {

    private final RequestConverter requestConverter;

    private final ResponseConverter responseConverter;

    private final WarehouseService warehouseService;

    private final ProductPriceService productPriceService;

    private final DataConverter dataConverter;

    @ApiOperation(
            httpMethod = "POST",
            value = "Поиск продуктов на складах"
    )
    @PostMapping("findProducts")
    public ResponseDto<ProductsFindResponseDto> findProducts(
            @ApiParam(
                    required = true,
                    value = "Запрос на поиск с фильтрами по названию и полям адреса"
            )
            @RequestBody @Valid final ProductsFindRequestDto requestDto) throws ServiceException {
        // Gather information from DB
        SearchFilter filter = requestConverter.fromDto(requestDto);
        List<ProductStoragePlace> productStoragePlaces = warehouseService.findProducts(filter);

        // Get additional information from remote service
        Set<Long> productIds = dataConverter.toProductId(productStoragePlaces);
        Map<Long, BigDecimal> priceMap = productPriceService.getPrice(productIds);

        // Combine all information into final result
        List<ProductInfoDto> productInfoDtos = responseConverter.toProductInfoDto(productStoragePlaces, priceMap);
        return ResponseDto.of(
                new ProductsFindResponseDto(productInfoDtos)
        );
    }
}
