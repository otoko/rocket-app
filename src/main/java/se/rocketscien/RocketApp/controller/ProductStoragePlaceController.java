package se.rocketscien.RocketApp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.RocketApp.dto.request.ProductMoveRequestDto;
import se.rocketscien.RocketApp.dto.request.ProductPutRequestDto;
import se.rocketscien.RocketApp.dto.request.ProductTakeRequestDto;
import se.rocketscien.RocketApp.dto.response.ProductMoveResponseDto;
import se.rocketscien.RocketApp.dto.response.ProductPutResponseDto;
import se.rocketscien.RocketApp.dto.response.ProductTakeResponseDto;
import se.rocketscien.RocketApp.service.WarehouseService;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import javax.validation.Valid;

@RestController
@RequestMapping("productStoragePlace")
@RequiredArgsConstructor
public class ProductStoragePlaceController {

    private final WarehouseService warehouseService;

    @ApiOperation(
            httpMethod = "POST",
            value = "Добавление продукта на место хранения"
    )
    @PostMapping("putProduct")
    public ResponseDto<ProductPutResponseDto> putProduct(
            @ApiParam(
                    required = true,
                    value = "Запрос с указанием места хранения, а также продукта и его количества"
            )
            @RequestBody @Valid final ProductPutRequestDto requestDto) throws ServiceException {
        warehouseService.putProduct(
                requestDto.getProductId(),
                requestDto.getStoragePlaceId(),
                requestDto.getAmount()
        );
        return ResponseDto.of(
                new ProductPutResponseDto()
        );
    }

    @ApiOperation(
            httpMethod = "POST",
            value = "Изъятие продукта из места хранения"
    )
    @PostMapping("takeProduct")
    public ResponseDto<ProductTakeResponseDto> takeProduct(
            @ApiParam(
                    required = true,
                    value = "Запрос с указанием места хранения, а также продукта и его количества"
            )
            @RequestBody @Valid final ProductTakeRequestDto requestDto) throws ServiceException {
        warehouseService.takeProduct(
                requestDto.getProductId(),
                requestDto.getStoragePlaceId(),
                requestDto.getAmount()
        );
        return ResponseDto.of(
                new ProductTakeResponseDto()
        );
    }

    @ApiOperation(
            httpMethod = "POST",
            value = "Перемещение продукта между местами хранения"
    )
    @PostMapping("moveProduct")
    public ResponseDto<ProductMoveResponseDto> moveProduct(
            @ApiParam(
                    required = true,
                    value = "Запрос с указанием мест хранения, а также продукта и его количества"
            )
            @RequestBody @Valid final ProductMoveRequestDto requestDto)
            throws ServiceException {
        warehouseService.moveProduct(
                requestDto.getProductId(),
                requestDto.getStoragePlaceFromId(),
                requestDto.getStoragePlaceToId(),
                requestDto.getAmount()
        );
        return ResponseDto.of(
                new ProductMoveResponseDto()
        );
    }
}
