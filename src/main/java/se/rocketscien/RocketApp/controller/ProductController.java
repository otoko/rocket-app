package se.rocketscien.RocketApp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.RocketApp.converter.ResponseConverter;
import se.rocketscien.RocketApp.dto.request.ProductCreateRequestDto;
import se.rocketscien.RocketApp.dto.response.ProductCreateResponseDto;
import se.rocketscien.RocketApp.entity.Product;
import se.rocketscien.RocketApp.service.WarehouseService;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import javax.validation.Valid;

@RestController
@RequestMapping("product")
@RequiredArgsConstructor
public class ProductController {

    private final WarehouseService warehouseService;

    private final ResponseConverter responseConverter;

    @ApiOperation(
            httpMethod = "POST",
            value = "Заведение нового продукта в системе"
    )
    @PostMapping("create")
    public ResponseDto<ProductCreateResponseDto> createProduct(
            @ApiParam(
                    required = true,
                    value = "Запрос с данными о продукте"
            )
            @RequestBody @Valid final ProductCreateRequestDto requestDto) throws ServiceException {
        Product result = warehouseService.addProduct(
                requestDto.getName()
        );
        return ResponseDto.of(
                responseConverter.toDto(result)
        );
    }

}
