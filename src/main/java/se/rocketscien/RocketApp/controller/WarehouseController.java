package se.rocketscien.RocketApp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.RocketApp.converter.ResponseConverter;
import se.rocketscien.RocketApp.dto.request.WarehouseCreateRequestDto;
import se.rocketscien.RocketApp.dto.request.WarehouseUpdateRequestDto;
import se.rocketscien.RocketApp.dto.response.WarehouseCreateResponseDto;
import se.rocketscien.RocketApp.dto.response.WarehouseUpdateResponseDto;
import se.rocketscien.RocketApp.entity.Warehouse;
import se.rocketscien.RocketApp.service.WarehouseService;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import javax.validation.Valid;

@RestController
@RequestMapping("warehouse")
@RequiredArgsConstructor
public class WarehouseController {

    private final WarehouseService warehouseService;

    private final ResponseConverter responseConverter;

    @ApiOperation(
            httpMethod = "POST",
            value = "Заведение нового склада в системе"
    )
    @PostMapping("create")
    public ResponseDto<WarehouseCreateResponseDto> createWarehouse(
            @ApiParam(
                    required = true,
                    value = "Запрос с параметрами склада"
            )
            @RequestBody @Valid final WarehouseCreateRequestDto requestDto) throws ServiceException {
        Warehouse warehouse = warehouseService.createWarehouse(
                requestDto.getAddressId()
        );
        return ResponseDto.of(
                responseConverter.toDto(warehouse)
        );
    }

    @ApiOperation(
            httpMethod = "POST",
            value = "Обновление параметров склада"
    )
    @PostMapping("update")
    public ResponseDto<WarehouseUpdateResponseDto> updateWarehouse(
            @ApiParam(
                    required = true,
                    value = "Запрос с новыми параметрами склада"
            )
            @RequestBody @Valid final WarehouseUpdateRequestDto requestDto) throws ServiceException {
        warehouseService.updateWarehouse(
                requestDto.getWarehouseId(),
                requestDto.getAddressId()
        );
        return ResponseDto.of(
                new WarehouseUpdateResponseDto()
        );
    }
}
