package se.rocketscien.RocketApp.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.rocketscien.RocketApp.converter.ResponseConverter;
import se.rocketscien.RocketApp.dto.request.AddressCreateRequestDto;
import se.rocketscien.RocketApp.dto.response.AddressCreateResponseDto;
import se.rocketscien.RocketApp.entity.Address;
import se.rocketscien.RocketApp.service.WarehouseService;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import javax.validation.Valid;

@RestController
@RequestMapping("address")
@RequiredArgsConstructor
public class AddressController {

    private final WarehouseService warehouseService;

    private final ResponseConverter responseConverter;

    @ApiOperation(
            httpMethod = "POST",
            value = "Заведение нового адреса в системе"
    )
    @PostMapping("create")
    public ResponseDto<AddressCreateResponseDto> createAddress(
            @ApiParam(
                    required = true,
                    value = "Запрос с полями адреса"
            )
            @RequestBody @Valid final AddressCreateRequestDto requestDto) throws ServiceException {
        Address result = warehouseService.createAddress(
                requestDto.getCity(),
                requestDto.getStreet(),
                requestDto.getHouse()
        );
        return ResponseDto.of(
                responseConverter.toDto(result)
        );
    }
}
