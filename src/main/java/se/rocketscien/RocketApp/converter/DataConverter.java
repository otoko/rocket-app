package se.rocketscien.RocketApp.converter;

import org.mapstruct.Mapper;
import se.rocketscien.RocketApp.entity.ProductStoragePlace;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface DataConverter {

    default Long toProductId(ProductStoragePlace entity) {
        return entity.getProduct().getId();
    }

    Set<Long> toProductId(List<ProductStoragePlace> list);
}
