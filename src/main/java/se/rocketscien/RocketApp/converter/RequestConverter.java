package se.rocketscien.RocketApp.converter;

import org.mapstruct.Mapper;
import se.rocketscien.RocketApp.dto.request.ProductsFindRequestDto;
import se.rocketscien.RocketApp.service.search.SearchFilter;

@Mapper(componentModel = "spring")
public interface RequestConverter {

    SearchFilter fromDto(ProductsFindRequestDto requestDto);
}
