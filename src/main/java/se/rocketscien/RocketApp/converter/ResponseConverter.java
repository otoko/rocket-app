package se.rocketscien.RocketApp.converter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import se.rocketscien.RocketApp.dto.ProductInfoDto;
import se.rocketscien.RocketApp.dto.response.AddressCreateResponseDto;
import se.rocketscien.RocketApp.dto.response.ProductCreateResponseDto;
import se.rocketscien.RocketApp.dto.response.StoragePlaceCreateResponseDto;
import se.rocketscien.RocketApp.dto.response.WarehouseCreateResponseDto;
import se.rocketscien.RocketApp.entity.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ResponseConverter {

    StoragePlaceCreateResponseDto toDto(StoragePlace entity);

    WarehouseCreateResponseDto toDto(Warehouse entity);

    AddressCreateResponseDto toDto(Address entity);

    ProductCreateResponseDto toDto(Product entity);

    @Mapping(source = "entity.product.name", target = "productName")
    @Mapping(source = "entity.product.rating", target = "productRating")
    @Mapping(source = "price", target = "productPrice")
    @Mapping(source = "entity.storagePlace.id", target = "storagePlaceId")
    @Mapping(source = "entity.storagePlace.warehouse.id", target = "warehouseId")
    @Mapping(source = "entity.storagePlace.warehouse.address.city", target = "city")
    @Mapping(source = "entity.storagePlace.warehouse.address.street", target = "street")
    @Mapping(source = "entity.storagePlace.warehouse.address.house", target = "house")
    ProductInfoDto toProductInfoDto(ProductStoragePlace entity, BigDecimal price);

    default List<ProductInfoDto> toProductInfoDto(List<ProductStoragePlace> list, Map<Long, BigDecimal> map) {
        return list.stream()
                .map(
                        entity -> toProductInfoDto(
                                entity, map.get(
                                        entity.getProduct().getId()
                                )
                        )
                )
                .collect(Collectors.toList());
    }
}
