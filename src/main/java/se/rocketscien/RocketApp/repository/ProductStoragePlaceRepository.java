package se.rocketscien.RocketApp.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.rocketscien.RocketApp.entity.Product;
import se.rocketscien.RocketApp.entity.ProductStoragePlace;
import se.rocketscien.RocketApp.entity.StoragePlace;

import java.util.Optional;

@Repository
public interface ProductStoragePlaceRepository
        extends CrudRepository<ProductStoragePlace, ProductStoragePlace.PrimaryKey>, JpaSpecificationExecutor<ProductStoragePlace> {

    Optional<ProductStoragePlace> findByProductAndStoragePlace(Product product, StoragePlace storagePlace);
}
