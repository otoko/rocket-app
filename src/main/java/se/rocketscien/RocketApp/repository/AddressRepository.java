package se.rocketscien.RocketApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.rocketscien.RocketApp.entity.Address;

import java.util.Optional;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

    Optional<Address> findByCityIsAndStreetIsAndHouseIs(String street, String city, String house);
}
