package se.rocketscien.RocketApp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.rocketscien.RocketApp.entity.StoragePlace;
import se.rocketscien.RocketApp.entity.Warehouse;

import java.util.Optional;

@Repository
public interface StoragePlaceRepository extends CrudRepository<StoragePlace, Long> {

    Optional<StoragePlace> findByWarehouseIsAndRowNumberIsAndPlaceNumberIs(Warehouse warehouse, Integer rowNumber, Integer placeNumber);
}
