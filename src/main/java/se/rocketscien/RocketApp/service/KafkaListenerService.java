package se.rocketscien.RocketApp.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.rocketscien.RocketApp.entity.Product;
import se.rocketscien.RocketApp.repository.ProductRepository;
import se.rocketscien.RocketCommons.kafka.ProductRating;
import se.rocketscien.RocketCommons.util.RatingValidator;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaListenerService {

    private final ProductRepository productRepository;

    private static boolean isValidPayload(ProductRating payload) {
        return payload != null &&
                payload.getProductId() != null && payload.getProductId() > 0 &&
                RatingValidator.isValid(payload.getRating());
    }

    @KafkaListener(topics = "${kafka.topicName}")
    @Transactional
    public void listenTopic(@Payload ProductRating payload) {
        log.info("Received message with payload: " + payload);
        if (isValidPayload(payload)) {
            Optional<Product> optional = productRepository.findById(payload.getProductId());
            if (optional.isPresent()) {
                Product product = optional.get();
                product.setRating(payload.getRating());
                productRepository.save(product);
            } else {
                log.error("No product with Id " + payload.getProductId());
            }
        } else {
            log.error("Invalid payload: " + payload);
        }
    }
}
