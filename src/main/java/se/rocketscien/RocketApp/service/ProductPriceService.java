package se.rocketscien.RocketApp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import se.rocketscien.RocketApp.dto.price.PriceGetRequestDto;
import se.rocketscien.RocketApp.dto.price.PriceGetResponseDto;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.exception.ServiceException;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ProductPriceService {

    private final ProductPriceClient productPriceClient;

    public Map<Long, BigDecimal> getPrice(Set<Long> productIds) throws ServiceException {

        PriceGetRequestDto requestDto = new PriceGetRequestDto();
        requestDto.setProductIds(productIds);

        ResponseDto<PriceGetResponseDto> responseDto = productPriceClient.getPrice(requestDto);

        if (responseDto == null) {
            throw new ServiceException("price.service.error.no.data");
        }
        PriceGetResponseDto payload = responseDto.getPayload();
        if (payload == null) {
            throw new ServiceException("price.service.error.no.data");
        }
        Map<Long, BigDecimal> priceMap = payload.getPriceMap();
        if (priceMap == null || priceMap.size() != productIds.size() || !priceMap.keySet().containsAll(productIds)) {
            throw new ServiceException("price.service.error.incomplete.or.wrong.data");
        }
        return priceMap;
    }
}
