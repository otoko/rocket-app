package se.rocketscien.RocketApp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.rocketscien.RocketApp.entity.*;
import se.rocketscien.RocketApp.repository.*;
import se.rocketscien.RocketApp.service.search.SearchFilter;
import se.rocketscien.RocketCommons.exception.ServiceException;

import java.util.List;
import java.util.Optional;

import static se.rocketscien.RocketApp.service.search.SearchSpecs.generateSpecification;

@Service
@RequiredArgsConstructor
public class WarehouseService {

    private final AddressRepository addressRepository;

    private final WarehouseRepository warehouseRepository;

    private final StoragePlaceRepository storagePlaceRepository;

    private final ProductRepository productRepository;

    private final ProductStoragePlaceRepository productStoragePlaceRepository;

    @Transactional
    public Address createAddress(String city, String street, String house) throws ServiceException {
        Optional<Address> optional = addressRepository.findByCityIsAndStreetIsAndHouseIs(city, street, house);
        if (optional.isPresent()) {
            throw new ServiceException("already.exists.address");
        }
        return addressRepository.save(new Address(city, street, house));
    }

    @Transactional
    public Warehouse createWarehouse(Long addressId) throws ServiceException {
        Address address = findAddressById(addressId);
        return warehouseRepository.save(new Warehouse(address));
    }

    private Address findAddressById(Long addressId) throws ServiceException {
        return addressRepository.findById(addressId)
                .orElseThrow(() -> new ServiceException("not.found.address"));
    }

    @Transactional
    public void updateWarehouse(Long warehouseId, Long addressId) throws ServiceException {
        Address address = findAddressById(addressId);
        Warehouse warehouse = findWarehouseById(warehouseId);
        warehouse.setAddress(address);
        warehouseRepository.save(warehouse);
    }

    private Warehouse findWarehouseById(Long warehouseId) throws ServiceException {
        return warehouseRepository.findById(warehouseId)
                .orElseThrow(() -> new ServiceException("not.found.warehouse"));
    }

    @Transactional
    public StoragePlace addStoragePlace(Long warehouseId, int rowNumber, int placeNumber) throws ServiceException {
        Warehouse warehouse = findWarehouseById(warehouseId);
        Optional<StoragePlace> optional =
                storagePlaceRepository
                        .findByWarehouseIsAndRowNumberIsAndPlaceNumberIs(warehouse, rowNumber, placeNumber);
        if (optional.isPresent()) {
            throw new ServiceException("already.exists.storage.place");
        }
        return storagePlaceRepository.save(new StoragePlace(warehouse, rowNumber, placeNumber));
    }

    @Transactional
    public Product addProduct(String name) throws ServiceException {
        Optional<Product> optional = productRepository.findByNameIs(name);
        if (optional.isPresent()) {
            throw new ServiceException("already.exists.product");
        }
        return productRepository.save(new Product(name));
    }

    @Transactional
    public void putProduct(Long productId, Long storagePlaceId, int amount) throws ServiceException {
        Product product = findProductById(productId);
        StoragePlace to = findStoragePlaceById(storagePlaceId);
        putProduct(product, to, amount);
    }

    private Product findProductById(Long productId) throws ServiceException {
        return productRepository.findById(productId)
                .orElseThrow(
                        () -> new ServiceException("not.found.product")
                );
    }

    private StoragePlace findStoragePlaceById(Long storagePlaceId) throws ServiceException {
        return storagePlaceRepository.findById(storagePlaceId)
                .orElseThrow(
                        () -> new ServiceException("not.found.storage.place")
                );
    }

    private void putProduct(Product product, StoragePlace to, int amount) {
        Optional<ProductStoragePlace> optional =
                productStoragePlaceRepository.findByProductAndStoragePlace(product, to);
        if (optional.isPresent()) {
            ProductStoragePlace record = optional.get();
            record.setProductAmount(record.getProductAmount() + amount);
            productStoragePlaceRepository.save(record);
        } else {
            productStoragePlaceRepository.save(new ProductStoragePlace(product, to, amount));
        }
    }

    @Transactional
    public void takeProduct(Long productId, Long storagePlaceId, int amount) throws ServiceException {
        Product product = findProductById(productId);
        StoragePlace from = findStoragePlaceById(storagePlaceId);
        takeProduct(product, from, amount);
    }

    private void takeProduct(Product product, StoragePlace from, int amount) throws ServiceException {
        ProductStoragePlace recordFrom
                = productStoragePlaceRepository.findByProductAndStoragePlace(product, from)
                .orElseThrow(
                        () -> new ServiceException("no.such.products.in.storage.place")
                );
        int productAmount = recordFrom.getProductAmount();
        if (productAmount < amount) {
            throw new ServiceException("not.enough.product.amount.in.storage.place");
        }
        if (productAmount == amount) {
            productStoragePlaceRepository.delete(recordFrom);
        } else {
            recordFrom.setProductAmount(productAmount - amount);
            productStoragePlaceRepository.save(recordFrom);
        }
    }

    @Transactional
    public void moveProduct(Long productId, Long fromId, Long toId, int amount) throws ServiceException {
        Product product = findProductById(productId);
        StoragePlace from = findStoragePlaceById(fromId);
        StoragePlace to = findStoragePlaceById(toId);
        takeProduct(product, from, amount);
        putProduct(product, to, amount);
    }

    public List<ProductStoragePlace> findProducts(SearchFilter filter) {
        return productStoragePlaceRepository.findAll(generateSpecification(filter));
    }
}
