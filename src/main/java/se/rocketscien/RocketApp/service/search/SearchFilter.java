package se.rocketscien.RocketApp.service.search;

import se.rocketscien.RocketApp.enums.SearchType;

import java.util.EnumMap;
import java.util.Map;

public class SearchFilter {

    private final EnumMap<SearchType, String> map;

    public SearchFilter() {
        map = new EnumMap<>(SearchType.class);
    }

    public SearchFilter(SearchFilter filter) {
        map = new EnumMap<>(SearchType.class);
        map.putAll(filter.map);
    }

    public SearchFilter(Map<SearchType, String> map) {
        this.map = new EnumMap<>(SearchType.class);
        this.map.putAll(map);
    }

    public void put(SearchType type, String value) {
        map.put(type, value);
    }

    public String get(SearchType type) {
        return map.get(type);
    }
}
