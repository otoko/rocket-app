package se.rocketscien.RocketApp.service.search;

import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;
import se.rocketscien.RocketApp.entity.Address;
import se.rocketscien.RocketApp.entity.Product;
import se.rocketscien.RocketApp.entity.ProductStoragePlace;
import se.rocketscien.RocketApp.entity.StoragePlace;
import se.rocketscien.RocketApp.enums.SearchType;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class SearchSpecs {

    private static String generateLikePattern(String s) {
        return "%" + s + "%";
    }

    public static Specification<ProductStoragePlace> generateSpecification(SearchFilter filter) {
        return Specification.where((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            String city = filter.get(SearchType.CITY);
            String street = filter.get(SearchType.STREET);
            if (city != null || street != null) {
                Join<StoragePlace, Address> joinAddress = root
                        .join("storagePlace")
                        .join("warehouse")
                        .join("address");
                if (city != null) {
                    predicates.add(
                            criteriaBuilder.like(joinAddress.get("city"), generateLikePattern(city))
                    );
                }
                if (street != null) {
                    predicates.add(
                            criteriaBuilder.like(joinAddress.get("street"), generateLikePattern(street))
                    );
                }
            }

            String productName = filter.get(SearchType.PRODUCT_NAME);
            if (productName != null) {
                Join<Product, String> joinProduct = root
                        .join("product");
                predicates.add(
                        criteriaBuilder.like(joinProduct.get("name"), generateLikePattern(productName))
                );
            }

            Predicate result = criteriaBuilder.conjunction();
            for (Predicate p : predicates) {
                result = criteriaBuilder.and(result, p);
            }
            return result;
        });
    }
}
