package se.rocketscien.RocketApp.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import se.rocketscien.RocketApp.dto.price.PriceGetRequestDto;
import se.rocketscien.RocketApp.dto.price.PriceGetResponseDto;
import se.rocketscien.RocketCommons.dto.ResponseDto;

@FeignClient(value = "productPrice", url = "${productPriceService.url}")
public interface ProductPriceClient {

    @PostMapping("getPrice")
    ResponseDto<PriceGetResponseDto> getPrice(PriceGetRequestDto requestDto);
}
