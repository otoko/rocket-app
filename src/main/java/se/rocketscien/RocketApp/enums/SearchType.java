package se.rocketscien.RocketApp.enums;

public enum SearchType {
    CITY,
    STREET,
    PRODUCT_NAME
}
