package se.rocketscien.RocketApp.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductInfoDto {

    private String productName;

    private BigDecimal productPrice;

    private BigDecimal productRating;

    private Integer productAmount;

    private String city;

    private String street;

    private String house;

    private Long warehouseId;

    private Long storagePlaceId;
}
