package se.rocketscien.RocketApp.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public abstract class EntityResponseDto {

    @ApiModelProperty(
            value = "Id созданной сущности",
            example = "1"
    )
    private Long id;
}
