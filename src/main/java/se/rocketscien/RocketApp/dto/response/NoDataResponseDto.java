package se.rocketscien.RocketApp.dto.response;

import lombok.Data;

@Data
public abstract class NoDataResponseDto {

    private Long placeHolder;
}
