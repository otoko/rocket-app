package se.rocketscien.RocketApp.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import se.rocketscien.RocketApp.dto.ProductInfoDto;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductsFindResponseDto {

    @ApiModelProperty(
            value = "Результат поиска"
    )
    private List<ProductInfoDto> productInfoList;
}
