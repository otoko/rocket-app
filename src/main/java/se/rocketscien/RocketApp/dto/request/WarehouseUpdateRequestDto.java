package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class WarehouseUpdateRequestDto {

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Id склада",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Long warehouseId;

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Id адреса",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Long addressId;
}
