package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ProductPutRequestDto {

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Id продукта",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Long productId;

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Id места хранения",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Long storagePlaceId;

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "количество",
            example = "10"
    )
    @NotNull
    @Min(1)
    private Integer amount;
}
