package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import se.rocketscien.RocketApp.enums.SearchType;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
public class ProductsFindRequestDto {

    @ApiModelProperty(
            required = true,
            value = "Фильтры поиска"
    )
    @NotNull
    private Map<SearchType, String> filterMap;
}
