package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class StoragePlaceCreateRequestDto {

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Id склада",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Long warehouseId;

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Номер полки",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Integer rowNumber;

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Номер места",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Integer placeNumber;
}
