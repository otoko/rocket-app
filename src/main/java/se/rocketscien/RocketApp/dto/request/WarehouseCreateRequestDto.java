package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class WarehouseCreateRequestDto {

    @ApiModelProperty(
            required = true,
            allowableValues = "range[1, infinity]",
            value = "Id адреса",
            example = "1"
    )
    @NotNull
    @Min(1)
    private Long addressId;
}
