package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AddressCreateRequestDto {

    @ApiModelProperty(
            required = true,
            value = "город",
            example = "Novosibirsk"
    )
    @NotBlank
    private String city;

    @ApiModelProperty(
            required = true,
            allowEmptyValue = true,
            value = "улица",
            example = "Nikolaeva"
    )
    @NotNull
    private String street;

    @ApiModelProperty(
            required = true,
            value = "дом",
            example = "11"
    )
    @NotBlank
    private String house;
}
