package se.rocketscien.RocketApp.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ProductCreateRequestDto {

    @ApiModelProperty(
            required = true,
            value = "название",
            example = "Cool Product"
    )
    @NotBlank
    private String name;
}
