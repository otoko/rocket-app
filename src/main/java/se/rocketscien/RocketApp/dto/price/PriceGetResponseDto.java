package se.rocketscien.RocketApp.dto.price;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class PriceGetResponseDto {

    private Map<Long, BigDecimal> priceMap;
}
