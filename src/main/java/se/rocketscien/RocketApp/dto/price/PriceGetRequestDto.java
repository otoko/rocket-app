package se.rocketscien.RocketApp.dto.price;

import lombok.Data;

import java.util.Set;

@Data
public class PriceGetRequestDto {

    private Set<Long> productIds;
}
