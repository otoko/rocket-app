package se.rocketscien.RocketApp.handler;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import se.rocketscien.RocketCommons.dto.ErrorDto;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.enums.ErrorType;
import se.rocketscien.RocketCommons.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ControllerAdvice
@RequiredArgsConstructor
public class RestResponseEntityExceptionHandler {

    public static final String VALIDATION_UNKNOWN_CODE = "validation.unknown.code";

    private final MessageSource messageSource;

    private String tryGetMessage(String msg, Locale locale) {
        try {
            return messageSource.getMessage(msg, null, locale);
        } catch (NoSuchMessageException exception) {
            return msg;
        }
    }

    @ExceptionHandler(value = {ServiceException.class})
    protected ResponseEntity<ResponseDto<Void>> handleServiceException(ServiceException ex, Locale locale) {
        List<ErrorDto> errors = new ArrayList<>();
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage(tryGetMessage(ex.getMessage(), locale));
        errorDto.setType(ErrorType.SERVICE);
        errors.add(errorDto);

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ResponseDto.of(errors));
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    protected ResponseEntity<ResponseDto<Void>> handleException(MethodArgumentNotValidException ex, Locale locale) {
        List<ErrorDto> errors = new ArrayList<>();
        for (ObjectError objectError : ex.getBindingResult().getAllErrors()) {
            ErrorDto errorDto = new ErrorDto();
            String[] codes = objectError.getCodes();
            if (codes != null && codes.length > 0) {
                errorDto.setMessage(tryGetMessage(codes[0], locale));
                errorDto.setCode(codes[0]);
            } else {
                errorDto.setMessage(tryGetMessage(VALIDATION_UNKNOWN_CODE, locale));
                errorDto.setCode(VALIDATION_UNKNOWN_CODE);
            }
            errorDto.setType(ErrorType.VALIDATION);
            errors.add(errorDto);
        }
        return ResponseEntity
                .badRequest()
                .body(ResponseDto.of(errors));
    }
}
