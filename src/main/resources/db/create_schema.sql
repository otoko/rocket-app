DROP DATABASE IF EXISTS rocket_db;
CREATE DATABASE rocket_db
WITH OWNER = rocket_user
ENCODING = 'UTF8'
TABLESPACE = pg_default
LC_COLLATE = 'ru_RU.UTF-8'
LC_CTYPE = 'ru_RU.UTF-8'
CONNECTION LIMIT = -1;

GRANT ALL PRIVILEGES ON DATABASE rocket_db TO rocket_user;

-- При создании базы требуется выполнить под суперпользователем:
CREATE EXTENSION IF NOT EXISTS "pgcrypto" SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "pg_trgm" SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "dblink";
