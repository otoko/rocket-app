CREATE TABLE IF NOT EXISTS address
(
    id     BIGSERIAL NOT NULL,
    city   TEXT      NOT NULL,
    street TEXT      NOT NULL,
    house  TEXT      NOT NULL,
    CONSTRAINT pk_address_id PRIMARY KEY (id),
    CONSTRAINT uq_address_city_street_house UNIQUE (city, street, house)
);

COMMENT ON TABLE address IS 'Адрес';
COMMENT ON COLUMN address.id IS 'Уникальный идентификатор';
COMMENT ON COLUMN address.city IS 'Город';
COMMENT ON COLUMN address.street IS 'Улица';
COMMENT ON COLUMN address.house IS 'Дом';
