CREATE TABLE IF NOT EXISTS storage_place
(
    id           BIGSERIAL NOT NULL,
    warehouse_id BIGINT    NOT NULL,
    row_number   INT       NOT NULL,
    place_number INT       NOT NULL,
    CONSTRAINT pk_storage_place_id PRIMARY KEY (id),
    CONSTRAINT fk_storage_place_warehouse_id FOREIGN KEY (warehouse_id) REFERENCES warehouse (id)
);

COMMENT ON TABLE storage_place IS 'Место хранения';
COMMENT ON COLUMN storage_place.id IS 'Уникальный идентификатор';
COMMENT ON COLUMN storage_place.warehouse_id IS 'Уникальный идентификатор склада';
COMMENT ON COLUMN storage_place.row_number IS 'Номер ряда';
COMMENT ON COLUMN storage_place.place_number IS 'Номер места';
