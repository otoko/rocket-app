CREATE TABLE IF NOT EXISTS product
(
    id     BIGSERIAL NOT NULL,
    "name" TEXT      NOT NULL,
    CONSTRAINT pk_product_id PRIMARY KEY (id),
    CONSTRAINT uq_product_name UNIQUE ("name")
);

COMMENT ON TABLE product IS 'Продукт';
COMMENT ON COLUMN product.id IS 'Уникальный идентификатор';
COMMENT ON COLUMN product.name IS 'Название';
