CREATE TABLE IF NOT EXISTS product_storage_place
(
    product_id       BIGINT NOT NULL,
    storage_place_id BIGINT NOT NULL,
    product_amount   INT    NOT NULL,
    CONSTRAINT pk_product_id_storage_place_id PRIMARY KEY (product_id, storage_place_id),
    CONSTRAINT fk_product_storage_place_product_id FOREIGN KEY (product_id) REFERENCES product (id),
    CONSTRAINT fk_product_storage_place_storage_place_id FOREIGN KEY (storage_place_id) REFERENCES storage_place (id)
);

COMMENT ON TABLE product_storage_place IS 'Таблица связей Продуктов и Мест хранения';
COMMENT ON COLUMN product_storage_place.product_id IS 'Уникальный идентификатор продукта';
COMMENT ON COLUMN product_storage_place.storage_place_id IS 'Уникальный идентификатор места хранения';
COMMENT ON COLUMN product_storage_place.product_amount IS 'Количество продукта';
