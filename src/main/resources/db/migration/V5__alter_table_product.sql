ALTER TABLE product
    ADD COLUMN rating NUMERIC(2, 1);

COMMENT ON COLUMN product.rating IS 'Рейтинг';
