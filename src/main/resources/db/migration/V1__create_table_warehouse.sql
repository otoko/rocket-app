CREATE TABLE IF NOT EXISTS warehouse
(
    id         BIGSERIAL NOT NULL,
    address_id BIGINT    NOT NULL,
    CONSTRAINT pk_warehouse_id PRIMARY KEY (id),
    CONSTRAINT uq_warehouse_address FOREIGN KEY (address_id) REFERENCES address (id)
);

COMMENT ON TABLE warehouse IS 'Склад';
COMMENT ON COLUMN warehouse.id IS 'Уникальный идентификатор';
COMMENT ON COLUMN warehouse.address_id IS 'Уникальный идентификатор адреса';
