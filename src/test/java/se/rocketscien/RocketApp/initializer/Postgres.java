package se.rocketscien.RocketApp.initializer;

import lombok.experimental.UtilityClass;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

@UtilityClass
public class Postgres {

    public static final PostgreSQLContainer<?> psqlContainer = new PostgreSQLContainer<>("postgres:12.4");

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + psqlContainer.getJdbcUrl(),
                    "spring.datasource.username=" + psqlContainer.getUsername(),
                    "spring.datasource.password=" + psqlContainer.getPassword()
            ).applyTo(applicationContext);
        }
    }

}
