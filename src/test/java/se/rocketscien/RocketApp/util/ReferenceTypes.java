package se.rocketscien.RocketApp.util;

import lombok.experimental.UtilityClass;
import org.springframework.core.ParameterizedTypeReference;
import se.rocketscien.RocketApp.dto.response.*;
import se.rocketscien.RocketCommons.dto.ResponseDto;

@UtilityClass
public class ReferenceTypes {

    public static final ParameterizedTypeReference<ResponseDto<AddressCreateResponseDto>>
            ADDRESS_CREATE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<WarehouseCreateResponseDto>>
            WAREHOUSE_CREATE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<ProductCreateResponseDto>>
            PRODUCT_CREATE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<WarehouseUpdateResponseDto>>
            WAREHOUSE_UPDATE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<StoragePlaceCreateResponseDto>>
            STORAGE_PLACE_CREATE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<ProductPutResponseDto>>
            PRODUCT_PUT_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<ProductTakeResponseDto>>
            PRODUCT_TAKE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<ProductMoveResponseDto>>
            PRODUCT_MOVE_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };

    public static final ParameterizedTypeReference<ResponseDto<ProductsFindResponseDto>>
            PRODUCTS_FIND_TYPE_REFERENCE = new ParameterizedTypeReference<>() {
    };
}
