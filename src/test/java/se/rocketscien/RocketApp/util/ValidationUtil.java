package se.rocketscien.RocketApp.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketCommons.dto.ErrorDto;
import se.rocketscien.RocketCommons.dto.ResponseDto;
import se.rocketscien.RocketCommons.enums.ErrorType;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@UtilityClass
public class ValidationUtil {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T readJsonToObject(String fileName, Class<T> aClass) throws IOException {
        return objectMapper.readValue(
                IntegrationTestBase.class.getClassLoader().getResourceAsStream(fileName), aClass);
    }

    public static ErrorDto generateValidationError(String code) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setType(ErrorType.VALIDATION);
        errorDto.setCode(code);
        return errorDto;
    }

    public static <Request, Payload, Response extends ResponseDto<Payload>>
    List<ErrorDto> executeAndAssertErrors(TestRestTemplate testRestTemplate, String url, Request requestDto,
                                          ParameterizedTypeReference<Response> responseTypeReference) {
        HttpEntity<Request> httpEntity = new HttpEntity<>(requestDto);

        ResponseEntity<Response> responseEntity =
                testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, responseTypeReference);
        assertThat(responseEntity.getStatusCode())
                .isEqualTo(HttpStatus.BAD_REQUEST);

        Response responseDto = responseEntity.getBody();
        assertThat(responseDto)
                .isNotNull();

        List<ErrorDto> errors = responseDto.getErrors();
        assertThat(errors)
                .isNotNull()
                .isNotEmpty();
        return errors;
    }

    public static void assertErrorIgnoreMessage(List<ErrorDto> errors, ErrorDto error) {
        assertThat(errors)
                .singleElement()
                .usingRecursiveComparison(
                        RecursiveComparisonConfiguration.builder().withIgnoredFields("message").build()
                ).isEqualTo(error);
    }
}
