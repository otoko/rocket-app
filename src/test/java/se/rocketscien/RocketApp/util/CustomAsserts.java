package se.rocketscien.RocketApp.util;

import lombok.experimental.UtilityClass;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.springframework.data.repository.CrudRepository;
import se.rocketscien.RocketApp.entity.BaseEntity;

import java.util.Collection;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@UtilityClass
public class CustomAsserts {

    public static <T> void assertEntityCollection(Iterable<T> entities, Collection<T> expectedContains) {
        assertThat(entities)
                .isNotNull()
                .isNotEmpty()
                .containsAll(expectedContains)
                .size().isEqualTo(expectedContains.size());
    }

    public static <T> void assertEntity(T entity, T expected) {
        assertThat(entity)
                .isNotNull();
        assertThat(entity)
                .isEqualTo(expected);
    }

    public static <T extends BaseEntity> void assertEntityIgnoreId(T entity, T expected) {
        assertThat(entity)
                .isNotNull();
        assertThat(entity)
                .usingRecursiveComparison(
                        RecursiveComparisonConfiguration.builder().withIgnoredFields("id").build()
                ).isEqualTo(expected);
        assertThat(entity.getId())
                .isNotNull()
                .isGreaterThan(0);
    }

    public static <E, ID, R extends CrudRepository<E, ID>> E assertEntityExists(ID id, R repository) {
        assertThat(id)
                .isNotNull();
        Optional<E> optional = repository.findById(id);
        assertThat(optional)
                .isPresent();
        return optional.get();
    }

    public static <E, ID, R extends CrudRepository<E, ID>> void assertEntityNotExists(ID id, R repository) {
        assertThat(id)
                .isNotNull();
        Optional<E> optional = repository.findById(id);
        assertThat(optional)
                .isNotPresent();
    }
}
