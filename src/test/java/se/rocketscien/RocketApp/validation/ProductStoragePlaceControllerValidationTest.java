package se.rocketscien.RocketApp.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketApp.dto.request.ProductMoveRequestDto;
import se.rocketscien.RocketApp.dto.request.ProductPutRequestDto;
import se.rocketscien.RocketApp.dto.request.ProductTakeRequestDto;
import se.rocketscien.RocketCommons.dto.ErrorDto;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static se.rocketscien.RocketApp.util.ReferenceTypes.*;
import static se.rocketscien.RocketApp.util.ValidationUtil.*;

public class ProductStoragePlaceControllerValidationTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static ProductPutRequestDto generatePutRequest(Consumer<ProductPutRequestDto> consumer) throws IOException {
        ProductPutRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/productPutRequest.json",
                ProductPutRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    private static ProductTakeRequestDto generateTakeRequest(Consumer<ProductTakeRequestDto> consumer)
            throws IOException {
        ProductTakeRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/productTakeRequest.json",
                ProductTakeRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    private static ProductMoveRequestDto generateMoveRequest(Consumer<ProductMoveRequestDto> consumer)
            throws IOException {
        ProductMoveRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/productMoveRequest.json",
                ProductMoveRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    static Stream<Arguments> parametersPut() throws IOException {
        return Stream.of(
                Arguments.of(
                        generatePutRequest(dto -> dto.setProductId(null)),
                        generateValidationError("NotNull.productPutRequestDto.productId")
                ),
                Arguments.of(
                        generatePutRequest(dto -> dto.setProductId(0L)),
                        generateValidationError("Min.productPutRequestDto.productId")
                ),
                Arguments.of(
                        generatePutRequest(dto -> dto.setStoragePlaceId(null)),
                        generateValidationError("NotNull.productPutRequestDto.storagePlaceId")
                ),
                Arguments.of(
                        generatePutRequest(dto -> dto.setStoragePlaceId(0L)),
                        generateValidationError("Min.productPutRequestDto.storagePlaceId")
                ),
                Arguments.of(
                        generatePutRequest(dto -> dto.setAmount(null)),
                        generateValidationError("NotNull.productPutRequestDto.amount")
                ),
                Arguments.of(
                        generatePutRequest(dto -> dto.setAmount(0)),
                        generateValidationError("Min.productPutRequestDto.amount")
                )
        );
    }

    static Stream<Arguments> parametersTake() throws IOException {
        return Stream.of(
                Arguments.of(
                        generateTakeRequest(dto -> dto.setProductId(null)),
                        generateValidationError("NotNull.productTakeRequestDto.productId")
                ),
                Arguments.of(
                        generateTakeRequest(dto -> dto.setProductId(0L)),
                        generateValidationError("Min.productTakeRequestDto.productId")
                ),
                Arguments.of(
                        generateTakeRequest(dto -> dto.setStoragePlaceId(null)),
                        generateValidationError("NotNull.productTakeRequestDto.storagePlaceId")
                ),
                Arguments.of(
                        generateTakeRequest(dto -> dto.setStoragePlaceId(0L)),
                        generateValidationError("Min.productTakeRequestDto.storagePlaceId")
                ),
                Arguments.of(
                        generateTakeRequest(dto -> dto.setAmount(null)),
                        generateValidationError("NotNull.productTakeRequestDto.amount")
                ),
                Arguments.of(
                        generateTakeRequest(dto -> dto.setAmount(0)),
                        generateValidationError("Min.productTakeRequestDto.amount")
                )
        );
    }

    static Stream<Arguments> parametersMove() throws IOException {
        return Stream.of(
                Arguments.of(
                        generateMoveRequest(dto -> dto.setProductId(null)),
                        generateValidationError("NotNull.productMoveRequestDto.productId")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setProductId(0L)),
                        generateValidationError("Min.productMoveRequestDto.productId")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setStoragePlaceFromId(null)),
                        generateValidationError("NotNull.productMoveRequestDto.storagePlaceFromId")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setStoragePlaceFromId(0L)),
                        generateValidationError("Min.productMoveRequestDto.storagePlaceFromId")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setStoragePlaceToId(null)),
                        generateValidationError("NotNull.productMoveRequestDto.storagePlaceToId")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setStoragePlaceToId(0L)),
                        generateValidationError("Min.productMoveRequestDto.storagePlaceToId")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setAmount(null)),
                        generateValidationError("NotNull.productMoveRequestDto.amount")
                ),
                Arguments.of(
                        generateMoveRequest(dto -> dto.setAmount(0)),
                        generateValidationError("Min.productMoveRequestDto.amount")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("parametersPut")
    void testPut(ProductPutRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/productStoragePlace/putProduct",
                requestDto,
                PRODUCT_PUT_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }

    @ParameterizedTest
    @MethodSource("parametersTake")
    void testTake(ProductTakeRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/productStoragePlace/takeProduct",
                requestDto,
                PRODUCT_TAKE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }

    @ParameterizedTest
    @MethodSource("parametersMove")
    void testMove(ProductMoveRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/productStoragePlace/moveProduct",
                requestDto,
                PRODUCT_MOVE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }
}