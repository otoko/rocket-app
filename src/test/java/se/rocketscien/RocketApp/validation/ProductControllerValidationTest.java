package se.rocketscien.RocketApp.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketApp.dto.request.ProductCreateRequestDto;
import se.rocketscien.RocketCommons.dto.ErrorDto;

import java.util.List;
import java.util.stream.Stream;

import static se.rocketscien.RocketApp.util.ReferenceTypes.PRODUCT_CREATE_TYPE_REFERENCE;
import static se.rocketscien.RocketApp.util.ValidationUtil.*;

public class ProductControllerValidationTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    static Stream<Arguments> parameters() {
        ProductCreateRequestDto productCreateRequestDto1 = new ProductCreateRequestDto();

        ErrorDto errorDto = generateValidationError("NotBlank.productCreateRequestDto.name");

        ProductCreateRequestDto productCreateRequestDto2 = new ProductCreateRequestDto();
        productCreateRequestDto2.setName("");

        return Stream.of(
                Arguments.of(
                        productCreateRequestDto1,
                        errorDto
                ),
                Arguments.of(
                        productCreateRequestDto2,
                        errorDto
                )
        );
    }

    @ParameterizedTest
    @MethodSource("parameters")
    void test(ProductCreateRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/product/create",
                requestDto,
                PRODUCT_CREATE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }
}