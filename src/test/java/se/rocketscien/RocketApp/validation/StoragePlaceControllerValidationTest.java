package se.rocketscien.RocketApp.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketApp.dto.request.StoragePlaceCreateRequestDto;
import se.rocketscien.RocketCommons.dto.ErrorDto;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static se.rocketscien.RocketApp.util.ReferenceTypes.STORAGE_PLACE_CREATE_TYPE_REFERENCE;
import static se.rocketscien.RocketApp.util.ValidationUtil.*;

public class StoragePlaceControllerValidationTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static StoragePlaceCreateRequestDto generateRequest(Consumer<StoragePlaceCreateRequestDto> consumer)
            throws IOException {
        StoragePlaceCreateRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/storagePlaceCreateRequest.json",
                StoragePlaceCreateRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    static Stream<Arguments> parameters() throws IOException {
        return Stream.of(
                Arguments.of(
                        generateRequest(dto -> dto.setWarehouseId(null)),
                        generateValidationError("NotNull.storagePlaceCreateRequestDto.warehouseId")
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setWarehouseId(0L)),
                        generateValidationError("Min.storagePlaceCreateRequestDto.warehouseId")
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setRowNumber(null)),
                        generateValidationError("NotNull.storagePlaceCreateRequestDto.rowNumber")
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setRowNumber(0)),
                        generateValidationError("Min.storagePlaceCreateRequestDto.rowNumber")
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setPlaceNumber(null)),
                        generateValidationError("NotNull.storagePlaceCreateRequestDto.placeNumber")
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setPlaceNumber(0)),
                        generateValidationError("Min.storagePlaceCreateRequestDto.placeNumber")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("parameters")
    void test(StoragePlaceCreateRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/storagePlace/create",
                requestDto,
                STORAGE_PLACE_CREATE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }
}