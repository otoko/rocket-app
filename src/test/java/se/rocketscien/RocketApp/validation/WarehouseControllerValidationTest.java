package se.rocketscien.RocketApp.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketApp.dto.request.WarehouseCreateRequestDto;
import se.rocketscien.RocketApp.dto.request.WarehouseUpdateRequestDto;
import se.rocketscien.RocketCommons.dto.ErrorDto;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static se.rocketscien.RocketApp.util.ReferenceTypes.WAREHOUSE_CREATE_TYPE_REFERENCE;
import static se.rocketscien.RocketApp.util.ReferenceTypes.WAREHOUSE_UPDATE_TYPE_REFERENCE;
import static se.rocketscien.RocketApp.util.ValidationUtil.*;

public class WarehouseControllerValidationTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static WarehouseCreateRequestDto generateCreateRequest(Consumer<WarehouseCreateRequestDto> consumer)
            throws IOException {
        WarehouseCreateRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/warehouseCreateRequest.json",
                WarehouseCreateRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    private static WarehouseUpdateRequestDto generateUpdateRequest(Consumer<WarehouseUpdateRequestDto> consumer)
            throws IOException {
        WarehouseUpdateRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/warehouseUpdateRequest.json",
                WarehouseUpdateRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    static Stream<Arguments> parametersCreate() throws IOException {
        return Stream.of(
                Arguments.of(
                        generateCreateRequest(dto -> dto.setAddressId(null)),
                        generateValidationError("NotNull.warehouseCreateRequestDto.addressId")
                ),
                Arguments.of(
                        generateCreateRequest(dto -> dto.setAddressId(0L)),
                        generateValidationError("Min.warehouseCreateRequestDto.addressId")
                )
        );
    }

    static Stream<Arguments> parametersUpdate() throws IOException {
        return Stream.of(
                Arguments.of(
                        generateUpdateRequest(dto -> dto.setAddressId(null)),
                        generateValidationError("NotNull.warehouseUpdateRequestDto.addressId")
                ),
                Arguments.of(
                        generateUpdateRequest(dto -> dto.setAddressId(0L)),
                        generateValidationError("Min.warehouseUpdateRequestDto.addressId")
                ),
                Arguments.of(
                        generateUpdateRequest(dto -> dto.setWarehouseId(null)),
                        generateValidationError("NotNull.warehouseUpdateRequestDto.warehouseId")
                ),
                Arguments.of(
                        generateUpdateRequest(dto -> dto.setWarehouseId(0L)),
                        generateValidationError("Min.warehouseUpdateRequestDto.warehouseId")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("parametersCreate")
    void testCreate(WarehouseCreateRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/warehouse/create",
                requestDto,
                WAREHOUSE_CREATE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }

    @ParameterizedTest
    @MethodSource("parametersUpdate")
    void testUpdate(WarehouseUpdateRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/warehouse/update",
                requestDto,
                WAREHOUSE_UPDATE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }
}