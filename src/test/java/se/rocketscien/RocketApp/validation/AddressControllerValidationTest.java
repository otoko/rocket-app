package se.rocketscien.RocketApp.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketApp.dto.request.AddressCreateRequestDto;
import se.rocketscien.RocketCommons.dto.ErrorDto;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static se.rocketscien.RocketApp.util.ReferenceTypes.ADDRESS_CREATE_TYPE_REFERENCE;
import static se.rocketscien.RocketApp.util.ValidationUtil.*;

public class AddressControllerValidationTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static AddressCreateRequestDto generateRequest(Consumer<AddressCreateRequestDto> consumer)
            throws IOException {
        AddressCreateRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/addressCreateRequest.json",
                AddressCreateRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    static Stream<Arguments> parameters() throws IOException {
        ErrorDto cityError = generateValidationError("NotBlank.addressCreateRequestDto.city");
        ErrorDto streetError = generateValidationError("NotNull.addressCreateRequestDto.street");
        ErrorDto houseError = generateValidationError("NotBlank.addressCreateRequestDto.house");

        return Stream.of(
                Arguments.of(
                        generateRequest(dto -> dto.setCity(null)),
                        cityError
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setCity("")),
                        cityError
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setStreet(null)),
                        streetError
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setHouse(null)),
                        houseError
                ),
                Arguments.of(
                        generateRequest(dto -> dto.setHouse("")),
                        houseError
                )
        );
    }

    @ParameterizedTest
    @MethodSource("parameters")
    void test(AddressCreateRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/address/create",
                requestDto,
                ADDRESS_CREATE_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }
}