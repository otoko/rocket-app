package se.rocketscien.RocketApp.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import se.rocketscien.RocketApp.IntegrationTestBase;
import se.rocketscien.RocketApp.dto.request.ProductsFindRequestDto;
import se.rocketscien.RocketCommons.dto.ErrorDto;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static se.rocketscien.RocketApp.util.ReferenceTypes.PRODUCTS_FIND_TYPE_REFERENCE;
import static se.rocketscien.RocketApp.util.ValidationUtil.*;

public class SearchControllerValidationTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private static ProductsFindRequestDto generateRequest(Consumer<ProductsFindRequestDto> consumer)
            throws IOException {
        ProductsFindRequestDto requestDto = readJsonToObject(
                "json/ValidationTest/productsFindRequest.json",
                ProductsFindRequestDto.class
        );
        consumer.accept(requestDto);
        return requestDto;
    }

    static Stream<Arguments> parameters() throws IOException {
        return Stream.of(
                Arguments.of(
                        generateRequest(dto -> dto.setFilterMap(null)),
                        generateValidationError("NotNull.productsFindRequestDto.filterMap")
                )
        );
    }

    @ParameterizedTest
    @MethodSource("parameters")
    void test(ProductsFindRequestDto requestDto, ErrorDto error) {

        List<ErrorDto> errors = executeAndAssertErrors(
                testRestTemplate,
                "/findProducts",
                requestDto,
                PRODUCTS_FIND_TYPE_REFERENCE
        );
        assertErrorIgnoreMessage(errors, error);
    }
}