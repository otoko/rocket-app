package se.rocketscien.RocketApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import se.rocketscien.RocketApp.dto.request.*;
import se.rocketscien.RocketApp.dto.response.ProductsFindResponseDto;
import se.rocketscien.RocketApp.entity.*;
import se.rocketscien.RocketApp.repository.*;
import se.rocketscien.RocketCommons.dto.ResponseDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static se.rocketscien.RocketApp.util.CustomAsserts.*;
import static se.rocketscien.RocketApp.util.ReferenceTypes.*;

public class RestTemplateTest extends IntegrationTestBase {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private StoragePlaceRepository storagePlaceRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductStoragePlaceRepository productStoragePlaceRepository;

    private WireMockServer wireMockServer;

    @BeforeEach
    public void setup() {
        wireMockServer = new WireMockServer(8090);
        wireMockServer.start();
    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    private <T> T readJsonToObject(String fileName, Class<T> aClass) throws IOException {
        return objectMapper.readValue(getClass().getClassLoader().getResourceAsStream(fileName), aClass);
    }

    private String readJsonToString(String fileName) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        assert inputStream != null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        return reader.lines()
                .collect(Collectors.joining("\n"));
    }

    private <Request, Payload, Response extends ResponseDto<Payload>>
    Payload executeAndAssertOk(String url, Request requestDto,
                               ParameterizedTypeReference<Response> responseTypeReference) {
        HttpEntity<Request> httpEntity = new HttpEntity<>(requestDto);

        ResponseEntity<Response> responseEntity =
                testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, responseTypeReference);
        assertThat(responseEntity.getStatusCode())
                .isEqualTo(HttpStatus.OK);

        Response responseDto = responseEntity.getBody();
        assertThat(responseDto)
                .isNotNull();
        assertThat(responseDto.getErrors())
                .isNullOrEmpty();

        Payload payload = responseDto.getPayload();
        assertThat(payload)
                .isNotNull();
        return payload;
    }

    private <Request, Payload, Response extends ResponseDto<Payload>>
    Payload executeAndAssertOk(String url, Class<Request> requestClass, String requestFileName,
                               ParameterizedTypeReference<Response> responseTypeReference) throws IOException {
        Request requestDto = readJsonToObject(
                requestFileName,
                requestClass
        );
        return executeAndAssertOk(url, requestDto, responseTypeReference);
    }

    private <Request, Payload, Response extends ResponseDto<Payload>>
    void executeAndAssertPayload(String url, Class<Request> requestClass, String requestFileName,
                                 ParameterizedTypeReference<Response> responseTypeReference,
                                 Class<Payload> payloadClass, String payloadFileName) throws IOException {
        Payload expectedPayloadDto = readJsonToObject(
                payloadFileName,
                payloadClass
        );

        Payload payload = executeAndAssertOk(url, requestClass, requestFileName, responseTypeReference);
        assertThat(payload)
                .isEqualTo(expectedPayloadDto);
    }

    @Test
    void test() throws IOException {

        // 1 step - Create address

        AddressCreateRequestDto addressCreateRequestDto = readJsonToObject(
                "json/RestTemplateTest/addressCreateRequest.json",
                AddressCreateRequestDto.class
        );
        Address expectedAddress = new Address(
                addressCreateRequestDto.getCity(),
                addressCreateRequestDto.getStreet(),
                addressCreateRequestDto.getHouse()
        );

        Long addressId = executeAndAssertOk(
                "/address/create",
                addressCreateRequestDto,
                ADDRESS_CREATE_TYPE_REFERENCE
        ).getId();
        Address address = assertEntityExists(addressId, addressRepository);
        assertEntityIgnoreId(address, expectedAddress);

        // 2 step - Create warehouse at this address

        Long warehouseId = executeAndAssertOk(
                "/warehouse/create",
                WarehouseCreateRequestDto.class,
                "json/RestTemplateTest/warehouseCreateRequest.json",
                WAREHOUSE_CREATE_TYPE_REFERENCE
        ).getId();

        Warehouse warehouse = assertEntityExists(warehouseId, warehouseRepository);
        assertThat(warehouse.getAddress().getId())
                .isEqualTo(addressId);

        // 3 step - Create second address

        AddressCreateRequestDto addressCreateRequestDto2 = readJsonToObject(
                "json/RestTemplateTest/addressCreateRequest2.json",
                AddressCreateRequestDto.class
        );
        Address expectedAddress2 = new Address(
                addressCreateRequestDto2.getCity(),
                addressCreateRequestDto2.getStreet(),
                addressCreateRequestDto2.getHouse()
        );

        Long addressId2 = executeAndAssertOk(
                "/address/create",
                addressCreateRequestDto2,
                ADDRESS_CREATE_TYPE_REFERENCE
        ).getId();

        Address address2 = assertEntityExists(addressId2, addressRepository);
        assertEntityIgnoreId(address2, expectedAddress2);

        // 4 step - Update warehouse with second address

        executeAndAssertOk(
                "/warehouse/update",
                WarehouseUpdateRequestDto.class,
                "json/RestTemplateTest/warehouseUpdateRequest.json",
                WAREHOUSE_UPDATE_TYPE_REFERENCE
        );

        Warehouse updatedWarehouse = assertEntityExists(warehouseId, warehouseRepository);
        assertThat(updatedWarehouse.getAddress().getId())
                .isEqualTo(addressId2);

        // 5 step - Create storage place

        StoragePlaceCreateRequestDto storagePlaceCreateRequestDto = readJsonToObject(
                "json/RestTemplateTest/storagePlaceCreateRequest.json",
                StoragePlaceCreateRequestDto.class
        );
        StoragePlace expectedStoragePlace = new StoragePlace(
                updatedWarehouse,
                storagePlaceCreateRequestDto.getRowNumber(),
                storagePlaceCreateRequestDto.getPlaceNumber()
        );

        Long storagePlaceId = executeAndAssertOk(
                "/storagePlace/create",
                storagePlaceCreateRequestDto,
                STORAGE_PLACE_CREATE_TYPE_REFERENCE
        ).getId();

        StoragePlace storagePlace = assertEntityExists(storagePlaceId, storagePlaceRepository);
        assertEntityIgnoreId(storagePlace, expectedStoragePlace);

        // 6 step - Create product

        ProductCreateRequestDto productCreateRequestDto = readJsonToObject(
                "json/RestTemplateTest/productCreateRequest.json",
                ProductCreateRequestDto.class
        );
        Product expectedProduct = new Product(
                productCreateRequestDto.getName()
        );

        Long productId = executeAndAssertOk(
                "/product/create",
                productCreateRequestDto,
                PRODUCT_CREATE_TYPE_REFERENCE
        ).getId();

        Product product = assertEntityExists(productId, productRepository);
        assertEntityIgnoreId(product, expectedProduct);

        // 7 step - Put product to storage place

        ProductPutRequestDto productPutRequestDto = readJsonToObject(
                "json/RestTemplateTest/productPutRequest.json",
                ProductPutRequestDto.class
        );
        Long sProductIdPut = productPutRequestDto.getProductId();
        Long sStoragePlaceIdPut = productPutRequestDto.getStoragePlaceId();
        ProductStoragePlace expectedProductStoragePlacePut = new ProductStoragePlace(
                assertEntityExists(sProductIdPut, productRepository),
                assertEntityExists(sStoragePlaceIdPut, storagePlaceRepository),
                productPutRequestDto.getAmount()
        );

        ProductStoragePlace.PrimaryKey primaryKeyPut = new ProductStoragePlace.PrimaryKey();
        primaryKeyPut.setProduct(sProductIdPut);
        primaryKeyPut.setStoragePlace(sStoragePlaceIdPut);

        assertEntityNotExists(primaryKeyPut, productStoragePlaceRepository);

        executeAndAssertOk(
                "/productStoragePlace/putProduct",
                productPutRequestDto,
                PRODUCT_PUT_TYPE_REFERENCE
        );

        ProductStoragePlace productStoragePlacePut = assertEntityExists(primaryKeyPut, productStoragePlaceRepository);
        assertEntity(productStoragePlacePut, expectedProductStoragePlacePut);

        // 8 step - Take product from storage place

        ProductTakeRequestDto productTakeRequestDto = readJsonToObject(
                "json/RestTemplateTest/productTakeRequest.json",
                ProductTakeRequestDto.class
        );

        ProductStoragePlace.PrimaryKey primaryKeyTake = new ProductStoragePlace.PrimaryKey();
        primaryKeyTake.setProduct(productTakeRequestDto.getProductId());
        primaryKeyTake.setStoragePlace(productTakeRequestDto.getStoragePlaceId());

        ProductStoragePlace pspTakeBefore = assertEntityExists(primaryKeyTake, productStoragePlaceRepository);

        executeAndAssertOk(
                "/productStoragePlace/takeProduct",
                productTakeRequestDto,
                PRODUCT_TAKE_TYPE_REFERENCE
        );

        ProductStoragePlace pspTakeAfter = assertEntityExists(primaryKeyTake, productStoragePlaceRepository);
        assertThat(pspTakeBefore.getProductAmount() - pspTakeAfter.getProductAmount())
                .isEqualTo(productTakeRequestDto.getAmount());

        // 9 step - Create second storage place

        StoragePlaceCreateRequestDto storagePlaceCreateRequestDto2 = readJsonToObject(
                "json/RestTemplateTest/storagePlaceCreateRequest2.json",
                StoragePlaceCreateRequestDto.class
        );
        StoragePlace expectedStoragePlace2 = new StoragePlace(
                updatedWarehouse,
                storagePlaceCreateRequestDto2.getRowNumber(),
                storagePlaceCreateRequestDto2.getPlaceNumber()
        );

        Long storagePlaceId2 = executeAndAssertOk(
                "/storagePlace/create",
                storagePlaceCreateRequestDto2,
                STORAGE_PLACE_CREATE_TYPE_REFERENCE
        ).getId();

        StoragePlace storagePlace2 = assertEntityExists(storagePlaceId2, storagePlaceRepository);
        assertEntityIgnoreId(storagePlace2, expectedStoragePlace2);

        // 10 step - Move product from first place to second place

        ProductMoveRequestDto productMoveRequestDto = readJsonToObject(
                "json/RestTemplateTest/productMoveRequest.json",
                ProductMoveRequestDto.class
        );
        Long sProductIdMove = productMoveRequestDto.getProductId();

        ProductStoragePlace.PrimaryKey primaryKeyMoveFrom = new ProductStoragePlace.PrimaryKey();
        primaryKeyMoveFrom.setProduct(sProductIdMove);
        primaryKeyMoveFrom.setStoragePlace(productMoveRequestDto.getStoragePlaceFromId());

        ProductStoragePlace pspMoveFromBefore = assertEntityExists(primaryKeyMoveFrom, productStoragePlaceRepository);

        ProductStoragePlace.PrimaryKey primaryKeyMoveTo = new ProductStoragePlace.PrimaryKey();
        primaryKeyMoveTo.setProduct(sProductIdMove);
        primaryKeyMoveTo.setStoragePlace(productMoveRequestDto.getStoragePlaceToId());

        assertEntityNotExists(primaryKeyMoveTo, productStoragePlaceRepository);

        executeAndAssertOk(
                "/productStoragePlace/moveProduct",
                productMoveRequestDto,
                PRODUCT_MOVE_TYPE_REFERENCE
        );

        ProductStoragePlace pspMoveFromAfter = assertEntityExists(primaryKeyMoveFrom, productStoragePlaceRepository);
        ProductStoragePlace pspMoveToAfter = assertEntityExists(primaryKeyMoveTo, productStoragePlaceRepository);
        assertThat(pspMoveToAfter.getProductAmount())
                .isEqualTo(pspMoveFromBefore.getProductAmount() - pspMoveFromAfter.getProductAmount())
                .isEqualTo(productMoveRequestDto.getAmount());

        // 11 step - Find products with filters

        wireMockServer.stubFor(post(urlEqualTo("/getPrice"))
                .withRequestBody(equalToJson(readJsonToString("__files/json/priceGetRequest.json")))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/priceGetResponse.json")));

        executeAndAssertPayload(
                "/findProducts",
                ProductsFindRequestDto.class,
                "json/RestTemplateTest/productsFindRequest.json",
                PRODUCTS_FIND_TYPE_REFERENCE,
                ProductsFindResponseDto.class,
                "json/RestTemplateTest/productsFindResponse.json"
        );

        wireMockServer.verify(postRequestedFor(urlEqualTo("/getPrice")));
    }
}
