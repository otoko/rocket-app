package se.rocketscien.RocketApp;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import se.rocketscien.RocketApp.entity.Product;
import se.rocketscien.RocketApp.repository.ProductRepository;
import se.rocketscien.RocketApp.service.KafkaListenerService;
import se.rocketscien.RocketCommons.kafka.ProductRating;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@Slf4j
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:9093", "port=9093"})
public class KafkaTest extends IntegrationTestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private KafkaTemplate<String, ProductRating> kafkaTemplate;

    @SpyBean
    private KafkaListenerService kafkaListenerService;

    private void send(ProductRating payload) {
        log.info("sending payload='{}' to topic='{}'", payload, "rating");
        kafkaTemplate.send("rating", payload);
    }

    @Test
    void test() {
        Product product = new Product();
        product.setName("product");
        product = productRepository.save(product);
        Long productId = product.getId();

        ProductRating payload = new ProductRating();
        payload.setProductId(productId);
        payload.setRating(BigDecimal.valueOf(0.5));

        send(payload);

        await().until(this::listenTopicCalled);

        Product updatedProduct = productRepository.findById(productId).get();
        assertThat(updatedProduct.getRating())
                .isNotNull()
                .isEqualByComparingTo(payload.getRating());
    }

    private Boolean listenTopicCalled() {
        try {
            verify(kafkaListenerService).listenTopic(any());
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
