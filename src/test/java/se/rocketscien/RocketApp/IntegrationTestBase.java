package se.rocketscien.RocketApp;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlGroup;
import se.rocketscien.RocketApp.initializer.Postgres;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {
        Postgres.Initializer.class
})
@SqlConfig(transactionMode = SqlConfig.TransactionMode.ISOLATED)
@SqlGroup({
        @Sql(scripts = {"/sql/db_clean_and_seq_reset.sql"},
                executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
        @Sql(scripts = {"/sql/db_clean_and_seq_reset.sql"},
                executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
})
public abstract class IntegrationTestBase {

    @BeforeAll
    static void init() {
        Postgres.psqlContainer.start();
    }
}
