DELETE FROM product_storage_place;
DELETE FROM storage_place;
DELETE FROM product;
DELETE FROM warehouse;
DELETE FROM address;

ALTER SEQUENCE storage_place_id_seq RESTART WITH 1;
ALTER SEQUENCE product_id_seq RESTART WITH 1;
ALTER SEQUENCE warehouse_id_seq RESTART WITH 1;
ALTER SEQUENCE address_id_seq RESTART WITH 1;