Dockered postgres:

docker run --name rocket-app-postgres -e POSTGRES_DB=rocket_db -e POSTGRES_PASSWORD=12345 -e POSTGRES_USER=rocket_user -e PGDATA=/var/lib/postgresql/data -p 5432:5432 -v /custom/mount:/var/lib/postgresql -d postgres

TestContainers:

You must run tests under user with permissions to access docker.